+++
title = "Interfaces"
description = "D-Bus common interfaces."
template = "docs/page.html"
draft = false
weight = 40
sort_by = "weight"

[extra]
lead = "A collection of commonly available interfaces & projects, showing the wild and diverse usages of D-Bus."
toc = true
top = false
+++

_Feel free to edit this page to promote common or useful interfaces._

### BlueZ

Linux Bluetooth protocol stack.

👉 [doc link](https://git.kernel.org/pub/scm/bluetooth/bluez.git/tree/doc)

### Desktop Notifications

For applications to generate passive popups to notify the user.

👉 [spec link](https://specifications.freedesktop.org/notification-spec/notification-spec-latest.html)

### File Manager Interface

Interact with the desktop's file manager.

👉 [spec link](https://www.freedesktop.org/wiki/Specifications/file-manager-interface/)

### Flatpak & portals

Flatpak grants sandboxed applications talk access to names in the
org.freedesktop.portal.* prefix. One possible way to use the portal APIs is thus
just to make D-Bus calls.

👉 [API link](https://docs.flatpak.org/en/latest/portal-api-reference.html)

### Geoclue

Geoclue is a D-Bus service that provides location information. The goal of the
Geoclue project is to make creating location-aware applications as simple as
possible.

👉 [doc link](https://www.freedesktop.org/software/geoclue/docs/)


### GNOME

GNOME shell offers a number of D-Bus interfaces.

👉 [search provider](https://developer.gnome.org/documentation/tutorials/search-provider.html)

### Idle Inhibition

AKA `org.freedesktop.ScreenSaver`.

👉 [spec link](https://specifications.freedesktop.org/idle-inhibit-spec/latest/)

### libvirt-dbus

libvirt-dbus wraps libvirt API to provide a high-level object-oriented API
better suited for dbus-based applications.

👉 [project](https://gitlab.com/libvirt/libvirt-dbus/-/tree/master/)

### MPRIS

The _Media Player Remote Interfacing Specification_ is a standard D-Bus
interface which aims to provide a common programmatic API for controlling media
players.

👉 [spec link](https://specifications.freedesktop.org/mpris-spec/)

### NetworkManager

Make networking configuration and operation as painless and automatic as possible.

👉 [project](https://wiki.gnome.org/Projects/NetworkManager)

### oFono

oFono is a free software project for mobile telephony applications. It is built
on 3GPP standards and uses a high-level D-Bus API for use by telephony
applications.

👉 [doc link](https://git.kernel.org/pub/scm/network/ofono/ofono.git/tree/doc)

### PackageKit

PackageKit is a system designed to make installing and updating software on your
computer easier.

👉 [API link](https://www.freedesktop.org/software/PackageKit/gtk-doc/api-reference.html)

### PolicyKit

polkit provides an authorization API intended to be used by privileged programs
offering service to unprivileged programs. See the [polkit manual
page](https://www.freedesktop.org/software/polkit/docs/latest/polkit.8.html) for
the system architecture and big picture.
 
👉 [API link](https://www.freedesktop.org/software/polkit/docs/latest/eggdbus-interface-org.freedesktop.PolicyKit1.Authority.html)

### Power Profiles

The power profiles API is meant to be used by parts of the OS or desktop
environment to switch system power profiles based on user choice, or user
intent.

👉 [API link](https://hadess.fedorapeople.org/power-profiles-daemon-docs/gdbus-net.hadess.PowerProfiles.html)

### RealtimeKit

RealtimeKit is a D-Bus system service that changes the scheduling policy of user
processes/threads to SCHED_RR (i.e. realtime scheduling mode) on request.

👉 [project](https://github.com/heftig/rtkit)

### Secret Service

The Secrets API allows client applications to store secrets securely using a
service running in the user's login session.

👉 [spec link](https://specifications.freedesktop.org/secret-service/)

### Storaged / udisk

The Storaged project provides a daemon, tools and libraries to access and
manipulate disks and storage devices.

👉 [API link](http://storaged.org/doc/udisks2-api/latest/ref-dbus.html)

### Stratis

Easy to use local storage management for Linux.

👉 [API link](https://stratis-storage.github.io/DBusAPIReference.pdf)

### systemd

systemd (system and service manager) and its auxiliary daemons expose a number
of APIs over D-Bus.

👉 [API link](https://www.freedesktop.org/software/systemd/man/org.freedesktop.systemd1.html)

### UPower

UPower is an abstraction for enumerating power devices, listening to device
events and querying history and statistics.

👉 [API link](https://upower.freedesktop.org/docs/ref-dbus.html)
