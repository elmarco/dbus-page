+++
title = "Specification"
description = "Link to specification and reference documents."
date = 2021-05-01T18:10:00+00:00
updated = 2021-05-01T18:10:00+00:00
draft = false
weight = 33
sort_by = "weight"
template = "docs/page.html"
+++


The specification is part of the reference implementation project.

The source document location is
[**here**](https://gitlab.freedesktop.org/dbus/dbus/-/blob/master/doc/dbus-specification.xml),
on freedesktop.org gitlab.

## 👉 [D-Bus specification](https://dbus.freedesktop.org/doc/dbus-specification.html)

