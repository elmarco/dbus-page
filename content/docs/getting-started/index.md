+++
title = "Getting started"
description = "First steps with D-Bus"
draft = false
weight = 30
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "First steps with D-Bus"
toc = true
top = false
+++

_Most desktops running D-Bus have the reference implementation installed. If the
following example fails, try with a more common desktop and distribution._

## Introduction

D-Bus introduces a few concepts for inter-processes communications.

Here is one way to make a method call from the shell:

```
dbus-send                     \
  --system                    \
  --type=method_call          \
  --print-reply               \
  --dest=org.freedesktop.DBus \
  /org/freedesktop/DBus       \
  org.freedesktop.DBus.ListNames
```

👉 You should get a list of _unique_ and _well-known_ names printed. Those are
_broadly speaking_ registered participants (connections and services) to the bus.

By reviewing the `dbus-send`[^1] arguments, let us introduce the main concepts:

- `--system`: connect to the **system bus**. A bus is a space for different
  services (or processes) to communicate. The system bus is unique per host and
  should always be available.
  
- `--type=method_call`: send a **method call** message (other messages exist,
  such as replies, signals).
  
- `--print-reply`: wait for the reply, print it.

- `--dest=org.freedesktop.DBus`: specify the **destination** for the message. It
  may be a _unique_ name (ex `:1.57`, identifying a specific connection), or a
  _well-known_ name which is resolved to a specific connection. You may think of
  it as DNS resolution, mapping the URL server address to an IP.
  
- `/org/freedesktop/DBus`: the **object path**, identifying a resource within
  the destination. You can also make the analogy with an HTTP path.
  
- `org.freedesktop.DBus.ListNames`: the interface (`org.freedesktop.DBus`) &
  method (`ListNames`) to call.
  
## Explore

D-Bus is most often used with a bus. The bus and services offer introspection
and monitoring.

You can explore the bus with a UI tool like
[d-feet](https://wiki.gnome.org/Apps/DFeet).

You can watch for messages on the bus with a monitor. For example, to watch for
the name activity on the bus, you may run `gdbus`[^2]:

```
gdbus monitor --session -d org.freedesktop.DBus
```

## Activation on demand

D-Bus offers a mechanism to start services only when needed. Thus, some services
(and processes) may not be running before you actually call or request them.
Read more about it in [the
specification](https://dbus.freedesktop.org/doc/dbus-specification.html#message-bus-starting-services).

## On buses and p2p

Two buses are commonly available to a user: the _system_ and the _session_
buses. When appropriate, private buses can also be running. They can be specific
to an application or a context.

But D-Bus is not limited to a bus topology. You may have processes talking to
each other _directly_ without going through a bus. This **peer-to-peer** mode
may allow better performances or security. It may be a better design for your
solution, and you may benefit from the available protocol implementations to
make your processes communicate.

## Read also

The [API Design
Guidelines](https://dbus.freedesktop.org/doc/dbus-api-design.html) for best
practices.

The [python.org wiki](https://wiki.python.org/moin/DbusExamples) on D-Bus, with
simple Python code examples.

The [project wiki](https://www.freedesktop.org/wiki/Software/dbus/) is full of
resources and documents (sometime a bit outdated).

[^1]: `dbus-send` is provided by the D-Bus reference implementation. 

[^2]: `gdbus` is provided by [glib](https://gitlab.gnome.org/GNOME/glib/).

[^3]: `busctl` is provided by [systemd](https://www.freedesktop.org/software/systemd/man/busctl.html).

[^4]: `qdbus` is provided by [qt](https://www.qt.io/)


