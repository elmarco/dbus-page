+++
title = "How to Contribute"
description = "Contribute to D-Bus."
draft = false
weight = 410
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Contribute to D-Bus."
toc = false
top = false
+++

## General discussions

The D-Bus mailing list is
[dbus@lists.freedesktop.org](https://lists.freedesktop.org/mailman/listinfo/dbus/);
discussion of protocol enhancements, new implementations, etc. should go there.

## Reference implementation

The reference implementation is hosted on [gitlab](https://gitlab.freedesktop.org/dbus/dbus).

> git clone https://gitlab.freedesktop.org/dbus/dbus

👉 Read [CONTRIBUTING](https://gitlab.freedesktop.org/dbus/dbus/-/blob/master/CONTRIBUTING.md).

👉 List or report an [issue](https://gitlab.freedesktop.org/dbus/dbus/-/issues).

👉 Make sure to read the [Code of Conduct](https://www.freedesktop.org/wiki/CodeOfConduct/).

## Other projects

D-Bus is implemented by various open-source projects. They will gladly
accept your help and contributions.

