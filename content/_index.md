+++
title = "D-Bus - desktop bus"


# The homepage contents
[extra]
lead = '<b>D-Bus</b> is the freedesktop.org message bus that lets your applications talk to one another for over 15 years.' 
url = "/docs/getting-started/"
url_button = "Get started"
repo_version = "Main project"
repo_license = "Open-source licenses."
repo_url = "https://gitlab.freedesktop.org/dbus"

[[extra.list]]
title = "Standard"
content = 'Provide <a href="https://dbus.freedesktop.org/doc/dbus-specification.html">standardize protocol</a> & <a href="interfaces">interfaces</a> for <a href="https://www.gnome.org/">GNOME</a>, <a href="https://kde.org/">KDE</a> and <a href="https://www.enlightenment.org/"/>various</a> <a href="https://www.xfce.org/"/>other</a> <a href="https://mate-desktop.org/"/>desktops</a>.'

[[extra.list]]
title = "Discoverable"
content = 'Services and interfaces can be <a href="docs/getting-started/#explore">discovered and introspected</a>, thanks to the bus and introspection capabilities.'

[[extra.list]]
title = "Polyglot"
content = 'Implementations and bindings available for <a href="docs/bindings"/>most programming languages</a>.'

[[extra.list]]
title = "Open"
content = 'Anyone can <a href="docs/contribute">participate</a>: define new interfaces, enhance the protocol, improve the implementations...'

[[extra.list]]
title = "Portable"
content = "Implementations are available for different systems and streams, not just Unix."

[[extra.list]]
title = "Friendly"
content = 'Various <a href="https://www.freedesktop.org/software/systemd/man/busctl.html">CLI</a> and <a href="https://wiki.gnome.org/Apps/DFeet">UI</a> <a href="https://doc.qt.io/qt-6/qdbusviewer.html">tools</a> are <a href="https://gitlab.freedesktop.org/bustle/bustle/-/wikis/home">available</a>, letting you discover and interact with D-Bus applications easily.'

+++
